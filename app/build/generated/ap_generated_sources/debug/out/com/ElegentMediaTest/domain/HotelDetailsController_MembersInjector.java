// Generated by dagger.internal.codegen.ComponentProcessor (https://google.github.io/dagger).
package com.ElegentMediaTest.domain;

import com.ElegentMediaTest.data.repository.HotelDetailsRepository;
import dagger.MembersInjector;
import javax.inject.Provider;

public final class HotelDetailsController_MembersInjector
    implements MembersInjector<HotelDetailsController> {
  private final Provider<HotelDetailsRepository> hotelDetailsRepositoryProvider;

  public HotelDetailsController_MembersInjector(
      Provider<HotelDetailsRepository> hotelDetailsRepositoryProvider) {
    assert hotelDetailsRepositoryProvider != null;
    this.hotelDetailsRepositoryProvider = hotelDetailsRepositoryProvider;
  }

  public static MembersInjector<HotelDetailsController> create(
      Provider<HotelDetailsRepository> hotelDetailsRepositoryProvider) {
    return new HotelDetailsController_MembersInjector(hotelDetailsRepositoryProvider);
  }

  @Override
  public void injectMembers(HotelDetailsController instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.hotelDetailsRepository = hotelDetailsRepositoryProvider.get();
  }

  public static void injectHotelDetailsRepository(
      HotelDetailsController instance,
      Provider<HotelDetailsRepository> hotelDetailsRepositoryProvider) {
    instance.hotelDetailsRepository = hotelDetailsRepositoryProvider.get();
  }
}
