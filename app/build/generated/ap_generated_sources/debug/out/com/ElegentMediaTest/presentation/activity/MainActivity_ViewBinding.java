// Generated code from Butter Knife. Do not modify!
package com.ElegentMediaTest.presentation.activity;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ElegentMediaTest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.tvMenuBarTitle = Utils.findRequiredViewAsType(source, R.id.tvMenuBarTitle, "field 'tvMenuBarTitle'", TextView.class);
    target.btnBack = Utils.findRequiredView(source, R.id.btnBack, "field 'btnBack'");
    target.rvHotelList = Utils.findRequiredViewAsType(source, R.id.rvHotelList, "field 'rvHotelList'", RecyclerView.class);
    target.tvUserName = Utils.findRequiredViewAsType(source, R.id.tvUserName, "field 'tvUserName'", TextView.class);
    target.tvEmail = Utils.findRequiredViewAsType(source, R.id.tvEmail, "field 'tvEmail'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvMenuBarTitle = null;
    target.btnBack = null;
    target.rvHotelList = null;
    target.tvUserName = null;
    target.tvEmail = null;
  }
}
