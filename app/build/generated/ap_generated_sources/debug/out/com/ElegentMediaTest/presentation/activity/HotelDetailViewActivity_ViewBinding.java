// Generated code from Butter Knife. Do not modify!
package com.ElegentMediaTest.presentation.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ElegentMediaTest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HotelDetailViewActivity_ViewBinding implements Unbinder {
  private HotelDetailViewActivity target;

  @UiThread
  public HotelDetailViewActivity_ViewBinding(HotelDetailViewActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HotelDetailViewActivity_ViewBinding(HotelDetailViewActivity target, View source) {
    this.target = target;

    target.tvMenuBarTitle = Utils.findRequiredViewAsType(source, R.id.tvMenuBarTitle, "field 'tvMenuBarTitle'", TextView.class);
    target.btnBack = Utils.findRequiredView(source, R.id.btnBack, "field 'btnBack'");
    target.ivHotelImage = Utils.findRequiredViewAsType(source, R.id.ivHotelImage, "field 'ivHotelImage'", ImageView.class);
    target.tvHotelViewTitle = Utils.findRequiredViewAsType(source, R.id.tvHotelViewTitle, "field 'tvHotelViewTitle'", TextView.class);
    target.tvHotelViewDescription = Utils.findRequiredViewAsType(source, R.id.tvHotelViewDescription, "field 'tvHotelViewDescription'", TextView.class);
    target.ivShowLocation = Utils.findRequiredViewAsType(source, R.id.ivShowLocation, "field 'ivShowLocation'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HotelDetailViewActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvMenuBarTitle = null;
    target.btnBack = null;
    target.ivHotelImage = null;
    target.tvHotelViewTitle = null;
    target.tvHotelViewDescription = null;
    target.ivShowLocation = null;
  }
}
