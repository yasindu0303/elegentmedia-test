// Generated code from Butter Knife. Do not modify!
package com.ElegentMediaTest.presentation.activity;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ElegentMediaTest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LocationViewActivity_ViewBinding implements Unbinder {
  private LocationViewActivity target;

  @UiThread
  public LocationViewActivity_ViewBinding(LocationViewActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LocationViewActivity_ViewBinding(LocationViewActivity target, View source) {
    this.target = target;

    target.tvMenuBarTitle = Utils.findRequiredViewAsType(source, R.id.tvMenuBarTitle, "field 'tvMenuBarTitle'", TextView.class);
    target.btnBack = Utils.findRequiredView(source, R.id.btnBack, "field 'btnBack'");
  }

  @Override
  @CallSuper
  public void unbind() {
    LocationViewActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvMenuBarTitle = null;
    target.btnBack = null;
  }
}
