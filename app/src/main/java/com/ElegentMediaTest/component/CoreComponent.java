package com.ElegentMediaTest.component;

import com.ElegentMediaTest.module.HotelDetailsModule;
import com.ElegentMediaTest.presentation.activity.HotelDetailViewActivity;
import com.ElegentMediaTest.presentation.activity.LocationViewActivity;
import com.ElegentMediaTest.presentation.activity.LoginActivity;
import com.ElegentMediaTest.presentation.activity.MainActivity;

import dagger.Component;

@Component(modules = {
        HotelDetailsModule.class,
})

public interface CoreComponent {

    void inject(LoginActivity loginActivity);

    void inject(MainActivity mainActivity);

    void inject(HotelDetailViewActivity hotelDetailViewActivity);

    void inject(LocationViewActivity locationViewActivity);

}
