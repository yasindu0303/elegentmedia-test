package com.ElegentMediaTest.component;

public class ComponentInjector {

    /**
     * Singleton instance.
     **/

    private static ComponentInjector instance;
    /**
     * Component instances.
     **/

    private CoreComponent coreComponent;
    private HotelDetailsComponent messagingComponent;

    public static ComponentInjector getInstance() {
        if (instance == null) {
            instance = new ComponentInjector();
        }
        return instance;
    }

    public CoreComponent getCoreComponent() {
        if (coreComponent == null) {
            coreComponent = DaggerCoreComponent.builder().build();
        }
        return coreComponent;
    }

    public HotelDetailsComponent getMessagingComponent() {
        if (messagingComponent == null) {
            messagingComponent = DaggerHotelDetailsComponent.builder().build();
        }
        return messagingComponent;
    }
}
