package com.ElegentMediaTest.component;

import com.ElegentMediaTest.data.repository.HotelDetailsRepository;
import com.ElegentMediaTest.domain.HotelDetailsController;
import com.ElegentMediaTest.module.HotelDetailsModule;

import dagger.Component;

@Component(modules = {HotelDetailsModule.class})
public interface HotelDetailsComponent {

    void inject(HotelDetailsController messagingController);

    void inject(HotelDetailsRepository messagingRepository);

}
