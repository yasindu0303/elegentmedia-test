package com.ElegentMediaTest.presentation.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ElegentMediaTest.presentation.UserInterfaceHandler;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class UserInterfaceActivity extends AppCompatActivity {

    private UserInterfaceHandler userInterfaceHandler;

    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        userInterfaceHandler = getUserInterfaceHandler();
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        if (userInterfaceHandler != null) {
            userInterfaceHandler.onDestroy();
        }
        super.onDestroy();
    }

    protected UserInterfaceHandler getUserInterfaceHandler() {
        if (this.userInterfaceHandler == null) {
            this.userInterfaceHandler = createUserInterfaceHandler();
        }
        return this.userInterfaceHandler;
    }

    protected UserInterfaceHandler createUserInterfaceHandler() {
        return new UserInterfaceHandler(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    protected void doTerminate() {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void setContentViewWithoutInject(int layoutResId) {
        super.setContentView(layoutResId);
    }
}
