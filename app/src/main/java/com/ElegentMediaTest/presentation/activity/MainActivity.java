package com.ElegentMediaTest.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.component.ComponentInjector;
import com.ElegentMediaTest.core.Constants;
import com.ElegentMediaTest.data.model.HotelDetails;
import com.ElegentMediaTest.data.model.UserProfile;
import com.ElegentMediaTest.data.model.request.GetHotelListRequest;
import com.ElegentMediaTest.domain.HotelDetailsController;
import com.ElegentMediaTest.presentation.adapter.HotelListAdapter;
import com.ElegentMediaTest.presentation.view.GeneralViewHandler;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends UserInterfaceActivity implements HotelListAdapter.IHotelDetailsReader {
    public static final int CODE_ACTIVITY_REQUEST_CODE = Constants.MainActivity;

    @Inject
    HotelDetailsController hotelDetailsController;

    @BindView(R.id.tvMenuBarTitle)
    TextView tvMenuBarTitle;
    @BindView(R.id.btnBack)
    View btnBack;
    @BindView(R.id.rvHotelList)
    RecyclerView rvHotelList;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvEmail)
    TextView tvEmail;

    UserProfile userProfile;
    AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken == null) {
                setResult(RESULT_OK);
                finish();
                Log.d("ElegentMEdiaTest", "new Logout");
            }
        }
    };
    private boolean active = false;
    private HotelListAdapter hotelListAdapter = new HotelListAdapter(MainActivity.this);
    private GeneralViewHandler<List<HotelDetails>> getHotelListViewHandler = new GeneralViewHandler<List<HotelDetails>>() {

        @Override
        public void onLoading() {
        }

        @Override
        public void onLoadingSuccess(List<HotelDetails> hotelDetails) {
            hotelListAdapter.setHotelDetails(hotelDetails);
            hotelListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onLoadingFailed(String error, String detail) {
            //getUserInterfaceHandler().displayError(getString(R.string.failed), detail);
        }

        @Override
        public void onSessionExpired(String message) {
        }
    };

    public static Intent getCallingIntent(Context context, UserProfile userProfile) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("userProfile", userProfile);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ComponentInjector.getInstance().getCoreComponent().inject(this);
        userProfile = (UserProfile) getIntent().getExtras().getSerializable("userProfile");
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;

        rvHotelList.setAdapter(hotelListAdapter);
        rvHotelList.setLayoutManager(new LinearLayoutManager(this));

        accessTokenTracker.startTracking();
        setupItemClickListner();
        setupView();
        gethotelList();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void gethotelList() {
        GetHotelListRequest getHotelListRequest = new GetHotelListRequest();
        hotelDetailsController.getHotelList(getHotelListRequest, getHotelListViewHandler);
    }

    @Override
    public void viewHotelDetails(HotelDetails hotelDetails) {
        startActivityForResult(HotelDetailViewActivity.getCallingIntent(getApplicationContext(), hotelDetails), HotelDetailViewActivity.CODE_ACTIVITY_REQUEST_CODE);
    }

    private void setupItemClickListner() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private void setupView() {
        tvUserName.setText(String.format("%s %s", userProfile.getFirstName(), userProfile.getLastName()));
        tvEmail.setText(userProfile.getEmail());
    }
}
