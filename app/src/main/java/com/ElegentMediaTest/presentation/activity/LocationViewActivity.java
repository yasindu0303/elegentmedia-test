package com.ElegentMediaTest.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.component.ComponentInjector;
import com.ElegentMediaTest.core.Constants;
import com.ElegentMediaTest.data.model.HotelDetails;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;

public class LocationViewActivity extends UserInterfaceActivity implements OnMapReadyCallback {
    public static final int CODE_ACTIVITY_REQUEST_CODE = Constants.LocationViewActivity;

    @BindView(R.id.tvMenuBarTitle)
    TextView tvMenuBarTitle;
    @BindView(R.id.btnBack)
    View btnBack;

    GoogleMap mGoogleMap;
    Marker mcurrent;
    LatLng latLng;
    SupportMapFragment mapFragment;
    private HotelDetails hotelDetails;

    public static Intent getCallingIntent(Context context, HotelDetails hotelDetails) {
        Intent intent = new Intent(context, LocationViewActivity.class);
        intent.putExtra("hotelDetails", hotelDetails);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_view);
        ComponentInjector.getInstance().getCoreComponent().inject(this);
        hotelDetails = (HotelDetails) getIntent().getExtras().getSerializable("hotelDetails");

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        tvMenuBarTitle.setText(getResources().getString(R.string.map));
        setupItemClickListeners();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        latLng = new LatLng(hotelDetails.getLatitude(), hotelDetails.getLongitude());
        mcurrent = mGoogleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)).title(hotelDetails.getAddress()));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }

    private void setupItemClickListeners() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}
