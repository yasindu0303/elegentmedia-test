package com.ElegentMediaTest.presentation.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.component.ComponentInjector;
import com.ElegentMediaTest.data.model.UserProfile;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;

public class LoginActivity extends UserInterfaceActivity {

    private static final String EMAIL = "email";
    @BindView(R.id.login_button)
    LoginButton loginButton;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ComponentInjector.getInstance().getCoreComponent().inject(this);

        checkLoginStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == MainActivity.CODE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_CANCELED) {
                setResult(RESULT_CANCELED);
                finish();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void loadUserProfile(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {
                    UserProfile userProfile = new UserProfile();
                    userProfile.setFirstName(object.getString("first_name"));
                    userProfile.setLastName(object.getString("last_name"));
                    userProfile.setEmail(object.getString("email"));

                    startActivityForResult(MainActivity.getCallingIntent(getApplicationContext(), userProfile), MainActivity.CODE_ACTIVITY_REQUEST_CODE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Bundle bundle = new Bundle();
        bundle.putString("fields", "first_name,last_name,email");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();
    }

    private void checkLoginStatus() {
        if (AccessToken.getCurrentAccessToken() != null) {
            loadUserProfile(AccessToken.getCurrentAccessToken());
        } else {
            loginButton.setReadPermissions(Arrays.asList(EMAIL));

            callbackManager = CallbackManager.Factory.create();
            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    loadUserProfile(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    // App code
                }

                @Override
                public void onError(FacebookException exception) {
                    // App code
                }
            });

        }
    }

}
