package com.ElegentMediaTest.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.component.ComponentInjector;
import com.ElegentMediaTest.core.Constants;
import com.ElegentMediaTest.data.model.HotelDetails;
import com.bumptech.glide.Glide;

import butterknife.BindView;

public class HotelDetailViewActivity extends UserInterfaceActivity {
    public static final int CODE_ACTIVITY_REQUEST_CODE = Constants.HotelDetailsViewActivity;

    @BindView(R.id.tvMenuBarTitle)
    TextView tvMenuBarTitle;
    @BindView(R.id.btnBack)
    View btnBack;
    @BindView(R.id.ivHotelImage)
    ImageView ivHotelImage;
    @BindView(R.id.tvHotelViewTitle)
    TextView tvHotelViewTitle;
    @BindView(R.id.tvHotelViewDescription)
    TextView tvHotelViewDescription;
    @BindView(R.id.ivShowLocation)
    ImageView ivShowLocation;

    private HotelDetails hotelDetails;

    public static Intent getCallingIntent(Context context, HotelDetails hotelDetails) {
        Intent intent = new Intent(context, HotelDetailViewActivity.class);
        intent.putExtra("hotelDetails", hotelDetails);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail_view);
        ComponentInjector.getInstance().getCoreComponent().inject(this);
        ivShowLocation.setVisibility(View.VISIBLE);

        hotelDetails = (HotelDetails) getIntent().getExtras().getSerializable("hotelDetails");
    }

    @Override
    protected void onStart() {
        super.onStart();
        tvMenuBarTitle.setText(getResources().getString(R.string.details));
        setupItemClickListner();
        setupView();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void setupItemClickListner() {

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        ivShowLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(LocationViewActivity.getCallingIntent(getApplicationContext(), hotelDetails), LocationViewActivity.CODE_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    private void setupView() {
        String imageUrl = hotelDetails.getImageLarge();

        Glide.with(this).load(imageUrl).placeholder(R.drawable.ic_img_placeholder).into(ivHotelImage);
        //   Picasso.get().load(hotelDetails.getImagemedium()).into(ivHotelImage);
        tvHotelViewTitle.setText(hotelDetails.getTitle());
        tvHotelViewDescription.setText(hotelDetails.getDescription());
    }
}
