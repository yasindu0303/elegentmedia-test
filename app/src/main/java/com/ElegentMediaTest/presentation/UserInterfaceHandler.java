package com.ElegentMediaTest.presentation;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.presentation.activity.UserInterfaceActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class UserInterfaceHandler {

    /**
     * The activity bound to this user interface handler.
     */
    private AppCompatActivity appCompatActivity;
    /**
     * Butterknife unbinder instance.
     */
    private Unbinder unbinder;
    /**
     * A handler instance used to post delayed ui events.
     */
    private Handler handler = new Handler();


    /**
     * Constructor.
     *
     * @param appCompatActivity the activity to be bound to this handler instance.
     */
    public UserInterfaceHandler(UserInterfaceActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
        this.unbinder = ButterKnife.bind(this, appCompatActivity);
    }

    /**
     * Destroys this handler instance.
     */
    public void onDestroy() {
        this.unbinder.unbind();
        this.appCompatActivity = null;
    }

    /**
     * Returns the activity bound to this instance.
     */
    public AppCompatActivity getAppCompatActivity() {
        return appCompatActivity;
    }
}
