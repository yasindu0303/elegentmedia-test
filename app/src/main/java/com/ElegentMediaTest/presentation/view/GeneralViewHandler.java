package com.ElegentMediaTest.presentation.view;

public interface GeneralViewHandler<T> {

    void onLoading();

    void onLoadingSuccess(T t);

    void onLoadingFailed(String error, String detail);

    void onSessionExpired(String message);

}
