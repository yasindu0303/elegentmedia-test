package com.ElegentMediaTest.presentation.adapter;

import android.content.Context;
import android.view.View;

import com.ElegentMediaTest.presentation.UserInterfaceHandler;

public interface IBucketDisplayAccessHandler {

    public Context getContext();


    public void setActionClickHandler(View view, Object obj);

    public UserInterfaceHandler getViewUserInterfaceHandler();

    public void userLoginSessionExpired(String message);

}