package com.ElegentMediaTest.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.core.Utiliz;
import com.ElegentMediaTest.data.model.HotelDetails;
import com.bumptech.glide.Glide;

import java.util.List;

public class HotelListAdapter extends
        RecyclerView.Adapter<HotelListAdapter.ViewHolder> {

    Context context;
    private IHotelDetailsReader hotelDetailsReader;
    private List<HotelDetails> hotelDetailsList;
    private HotelDetails hotelDetails;

    public HotelListAdapter(IHotelDetailsReader hotelDetailsReader) {
        this.hotelDetailsReader = hotelDetailsReader;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.listitem_hotels, parent, false);
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        hotelDetails = hotelDetailsList.get(position);

        if (!Utiliz.isEmpty(hotelDetails.getImageSmall())) {
            Glide.with(context).load(hotelDetails.getImageSmall()).placeholder(R.drawable.ic_img_placeholder).into(viewHolder.ivHotelIcon);
            //Picasso.get().load(hotelDetails.getImageSmall()).into(viewHolder.ivHotelIcon);
        }

        viewHolder.txHotelTitle.setText(hotelDetails.getTitle());
        viewHolder.txHotelAddress.setText(hotelDetails.getAddress());

        viewHolder.lvHotelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HotelDetails hotelDetails = hotelDetailsList.get(viewHolder.getAdapterPosition());
                hotelDetailsReader.viewHotelDetails(hotelDetails);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hotelDetailsList != null ? hotelDetailsList.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public List<HotelDetails> getHotelDetailsList() {
        return hotelDetailsList;
    }

    public void setHotelDetails(List<HotelDetails> hotelDetailsList) {

        if (this.hotelDetailsList != null) {
            this.hotelDetailsList.addAll(hotelDetailsList);
        } else {
            this.hotelDetailsList = hotelDetailsList;
        }
    }

    public void clearHotelDetailsList() {
        if (this.hotelDetailsList != null)
            this.hotelDetailsList.clear();
    }

    public interface IHotelDetailsReader {
        public void viewHotelDetails(HotelDetails hotelDetails);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        private LinearLayout lvHotelItem;
        private ImageView ivHotelIcon;
        private TextView txHotelTitle;
        private TextView txHotelAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            lvHotelItem = (LinearLayout) itemView.findViewById(R.id.lvHotelItem);
            ivHotelIcon = (ImageView) itemView.findViewById(R.id.ivHotelIcon);
            txHotelTitle = (TextView) itemView.findViewById(R.id.txHotelTitle);
            txHotelAddress = (TextView) itemView.findViewById(R.id.txHotelAddress);
        }
    }
}
