package com.ElegentMediaTest.module;

import com.ElegentMediaTest.component.ComponentInjector;
import com.ElegentMediaTest.data.repository.HotelDetailsRepository;
import com.ElegentMediaTest.data.service.HotelDetailsService;
import com.ElegentMediaTest.domain.HotelDetailsController;

import dagger.Module;
import dagger.Provides;

@Module
public class HotelDetailsModule {

    @Provides
    public HotelDetailsController getMessagingController() {
        HotelDetailsController messagingController = new HotelDetailsController();
        ComponentInjector.getInstance().getMessagingComponent().inject(messagingController);
        return messagingController;
    }

    @Provides
    public HotelDetailsRepository getMessagingRepository() {
        HotelDetailsRepository messagingRepository = new HotelDetailsRepository();
        ComponentInjector.getInstance().getMessagingComponent().inject(messagingRepository);
        return messagingRepository;
    }

    @Provides
    public HotelDetailsService getMessagingService() {
        HotelDetailsService messagingService = new HotelDetailsService();
        return messagingService;
    }

}
