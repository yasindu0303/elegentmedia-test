package com.ElegentMediaTest.core;

public class Constants {

    public static String LOG_TAG="ElegentMediaTest";

    public static final int LoginActivity = 10000;
    public static final int MainActivity = 10001;
    public static final int HotelDetailsViewActivity = 10002;
    public static final int LocationViewActivity = 10003;
}