package com.ElegentMediaTest.data.repository;

import com.ElegentMediaTest.data.model.request.GetHotelListRequest;
import com.ElegentMediaTest.data.model.response.GetHotelDetailsResponse;
import com.ElegentMediaTest.data.service.HotelDetailsService;

import javax.inject.Inject;

import io.reactivex.Observable;

public class HotelDetailsRepository {

    @Inject
    HotelDetailsService hotelDetailsService;

    public Observable<GetHotelDetailsResponse> sendMessage(final GetHotelListRequest getHotelListRequest) {
        return hotelDetailsService.getHotelDetails(getHotelListRequest);
    }
}
