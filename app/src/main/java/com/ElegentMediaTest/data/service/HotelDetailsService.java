package com.ElegentMediaTest.data.service;

import com.ElegentMediaTest.data.model.request.GetHotelListRequest;
import com.ElegentMediaTest.data.model.response.GetHotelDetailsResponse;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HotelDetailsService extends Service {

    public Observable<GetHotelDetailsResponse> getHotelDetails(final GetHotelListRequest getHotelListRequest) {
        return Observable.create(new ObservableOnSubscribe<GetHotelDetailsResponse>() {
            @Override
            public void subscribe(ObservableEmitter<GetHotelDetailsResponse> e) throws Exception {
                try {

                    Request request = getRequestBuilder()
                            .url(getRequestUrl("/s/6nt7fkdt7ck0lue/hotels.json"))
                            .get()
                            .build();

                    Response response = okHttpClient.newCall(request).execute();
                    String responseBody = response.body().string();

                    GetHotelDetailsResponse getHotelDetailsResponse = new GetHotelDetailsResponse();
                    switch (response.code()) {
                        case 200:
                            getHotelDetailsResponse.parseSuccessResponse(responseBody);
                            break;
                        case 400:
                        case 401:
                            getHotelDetailsResponse.parseErrorResponse(responseBody);
                            break;
                        default:
                            throw new UnsupportedOperationException();
                    }

                    // Handle all supported response codes.
                    if (!e.isDisposed()) {
                        e.onNext(getHotelDetailsResponse);
                        e.onComplete();
                    }

                } catch (IOException ex) {
                    // Handle any network errors.
                    if (!e.isDisposed()) {
                        e.onError(new RuntimeException(getNetworkErrorMessage()));
                    }

                } catch (Exception ex) {
                    // Handle any other response codes.
                    if (!e.isDisposed()) {
                        e.onError(new RuntimeException(getServerErrorMessage()));
                    }
                }
            }
        });
    }

}

