package com.ElegentMediaTest.data.service;


import com.ElegentMediaTest.BuildConfig;
import com.ElegentMediaTest.R;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;

public class Service {
    /**
     * OkHttp network client instance.
     */
    protected static final OkHttpClient okHttpClient;

    static {

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.COMPATIBLE_TLS));
        builder.connectTimeout(30, TimeUnit.SECONDS) // connect timeout
                .writeTimeout(30, TimeUnit.SECONDS) // write timeout
                .readTimeout(30, TimeUnit.SECONDS); // read timeout

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder = builder.addInterceptor(logging);
        }
        okHttpClient = builder.build();
    }

    protected String getRequestUrl(String path) {
        return BuildConfig.BASE_URL.concat(path);
    }

    protected Request.Builder getRequestBuilder() {
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.header("Content-Type", "application/json; charset=utf-8");
        return requestBuilder;
    }

    protected String getNetworkErrorMessage() {
        return "" + R.string.failed_to_connect;
    }

    protected String getServerErrorMessage() {
        return "" + R.string.something_went_wrong;
    }

}
