package com.ElegentMediaTest.data.model.request;

import org.json.JSONException;

public abstract class RequestDataModel {

    public abstract String getDataParams() throws JSONException;

}
