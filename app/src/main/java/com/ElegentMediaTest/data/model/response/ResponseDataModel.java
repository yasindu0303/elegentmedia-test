package com.ElegentMediaTest.data.model.response;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

public abstract class ResponseDataModel {

    private String errorCode;

    private String message;

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void parseErrorResponse(String response) throws JSONException {

        JSONObject jsonObject = new JSONObject(response);

        JSONObject errorObject = jsonObject.getJSONObject("error");

        errorCode = errorObject.getString("error_code");

        message = errorObject.getString("message");

    }


    public abstract void parseSuccessResponse(String response) throws JSONException, ParseException;


}
