package com.ElegentMediaTest.data.model.response;

import android.util.Log;

import com.ElegentMediaTest.data.model.HotelDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetHotelDetailsResponse extends ResponseDataModel {

    private List<HotelDetails> hotelList = new ArrayList<>();

    public List<HotelDetails> gethotelList() {
        return hotelList;
    }

    @Override
    public void parseSuccessResponse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray hotelDetailsArr = jsonObject.getJSONArray("data");

        Log.d("ElegentMedia1", "parseSuccessResponse:hotelDetailsArr.length() "+hotelDetailsArr.length());

        HotelDetails hotelDetails;
        JSONObject hotelDetailsObj;
        for (int idx = 0; idx < hotelDetailsArr.length(); idx++) {
            hotelDetailsObj = hotelDetailsArr.getJSONObject(idx);
            hotelDetails = new HotelDetails();
            hotelDetails.setId(hotelDetailsObj.getInt("id"));
            hotelDetails.setTitle(hotelDetailsObj.getString("title"));
            hotelDetails.setDescription(hotelDetailsObj.getString("description"));
            hotelDetails.setAddress(hotelDetailsObj.getString("address"));
            hotelDetails.setPostCode(hotelDetailsObj.getString("postcode"));
            hotelDetails.setPhoneNumber(hotelDetailsObj.getString("phoneNumber"));
            hotelDetails.setLatitude(hotelDetailsObj.getDouble("latitude"));
            hotelDetails.setLongitude(hotelDetailsObj.getDouble("longitude"));

            JSONObject imagObj = hotelDetailsObj.getJSONObject("image");
            hotelDetails.setImageSmall(imagObj.getString("small"));
            hotelDetails.setImagemedium(imagObj.getString("medium"));
            hotelDetails.setImageLarge(imagObj.getString("large"));

            hotelList.add(hotelDetails);

            Log.d("ElegentMedia1", "parseSuccessResponse:hotelList.size() "+hotelList.size());
        }

    }
}
