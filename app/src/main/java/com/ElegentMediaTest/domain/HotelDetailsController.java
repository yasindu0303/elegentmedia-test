package com.ElegentMediaTest.domain;

import com.ElegentMediaTest.R;
import com.ElegentMediaTest.data.model.HotelDetails;
import com.ElegentMediaTest.data.model.request.GetHotelListRequest;
import com.ElegentMediaTest.data.model.response.GetHotelDetailsResponse;
import com.ElegentMediaTest.data.repository.HotelDetailsRepository;
import com.ElegentMediaTest.presentation.view.GeneralViewHandler;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HotelDetailsController {

    @Inject
    HotelDetailsRepository hotelDetailsRepository;

    public void getHotelList(GetHotelListRequest getHotelListRequest, GeneralViewHandler<List<HotelDetails>> getHotelListViewHandler) {
        hotelDetailsRepository.sendMessage(getHotelListRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetHotelDetailsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(GetHotelDetailsResponse getHotelDetailsResponse) {
                        if (getHotelDetailsResponse.getErrorCode() == null) {
                            getHotelListViewHandler.onLoadingSuccess(getHotelDetailsResponse.gethotelList());
                        } else if (getHotelDetailsResponse.getErrorCode().equals("10014")) {
                            getHotelListViewHandler.onSessionExpired(getHotelDetailsResponse.getMessage());
                        } else {
                            getHotelListViewHandler.onLoadingFailed(String.valueOf(R.string.failed), getHotelDetailsResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                      //  generalViewHandler.onLoadingFailed("Error", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
